package com.hunt.survey;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SurveySubmitServlet
 */
@WebServlet("/surveys/v1")
public class SurveySubmitServlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String status = request.getParameter("status");
        String[] favorites = request.getParameterValues("favorites");

        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet submission</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet submit at " + request.getContextPath () + "</h1>");
            out.println("status was " + status + "<br/>");
            out.println("<ul>");
            for(String topic : favorites) {
                out.println("<li>" +  topic + "</li>");
            }
            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        } catch(Exception e) {
            out.println(e);
        } finally { 
            out.close();
        }
    } 
}

