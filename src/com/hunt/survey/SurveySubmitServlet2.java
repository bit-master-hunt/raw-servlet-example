package com.hunt.survey;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hunt.models.SurveySubmission;
import com.hunt.services.SurveyService;

@WebServlet("/surveys/v2")
public class SurveySubmitServlet2 extends HttpServlet {
	private static final long serialVersionUID = 6029710796926410115L;
	private SurveyService surveyService;
	
	private String documentHead = "  <head>\n" + 
			"    <meta http-equiv=\"Content-Type\" content=\"text/html\">\n" + 
			"    \n" + 
			"    <title>Survey</title>\n" + 
			"    <link rel=\"stylesheet\" href=\"css/survey.css\" type=\"text/css\" /> \n" + 
			"    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css\">\n" + 
			"    \n" + 
			"    <script\n" + 
			"       src=\"https://code.jquery.com/jquery-3.2.1.js\"\n" + 
			"       integrity=\"sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=\"\n" + 
			"       crossorigin=\"anonymous\">\n" + 
			"    </script>\n" + 
			"\n" + 
			"    <script src=\"js/survey.js\"></script>\n" + 
			"  </head>";
	

	
	public void init() {
		surveyService = SurveyService.getInstance();
	}
	
	public String getTable(Map<String, Integer> data) {
		String start = "<table class=\"table table-condensed\">";
		String headers = "<tr><th>Name</th><th>Count</th></tr>";
		String rows = "";
		String close = "</table>";
		for( String key : data.keySet() ) {
			rows += "<tr><td>" + key + "</td><td>" + data.get(key) + "</td></tr>";			
		}
				
		return start + headers + rows + close;
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String status = request.getParameter("status");
		List<String> favorites = Arrays.asList( request.getParameterValues("favorites") );

		SurveySubmission submission = 
				new SurveySubmission.Builder()
					.favorites(favorites)
					.status(status)
					.build();
		
		surveyService.submit( submission );
		writeHTML( response );
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		writeHTML(response);
	}
	
	public void writeHTML( HttpServletResponse response ) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();

		try {
			out.println("<html>");
			out.println( documentHead );
			out.println("<body class=\"container\">");
			
			out.println("<h2>Favorites</h2>");
			out.println( getTable(surveyService.getTopics() ) );
			
			out.println("<h2>Residential Status</h2>");
			out.println( getTable( surveyService.getStatuses() ) );
			
			out.println("</body>");
			out.println("</html>");
		} catch(Exception e) {
			out.println(e);
		} finally { 
			out.close();
		}	
	}

}
